# Alphabet Soup - How to Run
````
After cloning this fork, compiling and running this java program assumes JDK installation.
- Open a command prompt and go to alphabet-soup-csaldias/challenge/src/carlos/saldias/challenge/AlphabetSoupMainApplication.java
- Compile the program by typing:
 javac AlphabetSoupMainApplication.java
  If there are no errors, the command prompt will take you to the next line (Assumption: The JAVA_HOME path variable is set).
- Finally, to run your program, type:
 java AlphabetSoupMainApplication 
  
You will be able to see the result printed on the window.
````