package carlos.saldias.soup;

import java.util.List;

public class AlphabetSoupMainApplication {
    private static AlphabetSoupFactory factory;

    public static void main(String[] args) {
        factory = new AlphabetSoupBuilder();

        List<String> sourceFile = FileUtil.readInputFile("file1.txt");
        System.out.println("******************** File Source file1.txt ************************");
        factory.processAlphabetSoup(sourceFile);

        sourceFile = FileUtil.readInputFile("file2.txt");
        System.out.println("******************** File Source file2.txt ************************");
        factory.processAlphabetSoup(sourceFile);

        sourceFile = FileUtil.readInputFile("file3.txt");
        System.out.println("******************** File Source file3.txt ************************");
        factory.processAlphabetSoup(sourceFile);

    }
}
