package carlos.saldias.soup;

import java.util.ArrayList;
import java.util.List;

public class AlphabetSoupBuilder implements AlphabetSoupFactory{

    private int gridSize;
    private char[][] grid;
    private  List<String> results;

    @Override
    public void processAlphabetSoup(List<String> sourceFile) {
        AlphabetSoupHelper.describeFileSource(sourceFile);

        int gridSize = Integer.parseInt(sourceFile.get(0).split("x")[0]);

        List<String> theGrid = AlphabetSoupHelper.buildWordGrid(sourceFile,gridSize);
        List<String> words = AlphabetSoupHelper.wordsList(sourceFile, gridSize);

        List<String> results = findWords(theGrid, words);

        AlphabetSoupHelper.displayFoundWordsCoordinates(results);

    }

    private List<String> findWords(List<String> wordGrid, List<String> words) {
         gridSize = wordGrid.size();
         grid = new char[gridSize][gridSize];

        for (int i = 0; i < gridSize; i++) {
            String[] row = wordGrid.get(i).split(" ");
            for (int j = 0; j < gridSize; j++) {
                grid[i][j] = row[j].charAt(0);
            }
        }

        results = new ArrayList<>();

        for (String word : words) {
            int wordLen = word.length();

            for (int i = 0; i < gridSize; i++) {
                for (int j = 0; j < gridSize; j++) {
                    if (grid[i][j] == word.charAt(0)) {

                        // horizontal right
                        if (j + wordLen <= gridSize) {
                            //boolean found = true;
                            boolean found = doHorizontalRight(wordLen,word,i,j);
                            if (found) {
                                results.add(word + " " + i + ":" + j + " " + i + ":" + (j + wordLen - 1));
                                continue;
                            }
                        }

                        // horizontal left
                        if (j - wordLen >= -1) {
                            //boolean found = true;
                            boolean found = doHorizontalLeft(wordLen,word,i,j);
                            if (found) {
                                results.add(word + " " + i + ":" + j + " " + i + ":" + (j - wordLen + 1));
                                continue;
                            }
                        }

                        // vertical down
                        if (i + wordLen <= gridSize) {
                            //boolean found = true;
                            boolean found = doVerticalDown(wordLen,word,i,j);
                            if (found) {
                                results.add(word + " " + i + ":" + j + " " + (i + wordLen - 1) + ":" + j);
                                continue;
                            }
                        }

                        // vertical up
                        if (i - wordLen >= -1) {
                            //boolean found = true;
                            boolean found = doVerticalUp(wordLen,word,i,j);
                            if (found) {
                                results.add(word + " " + i + ":" + j + " " + (i - wordLen + 1) + ":" + j);
                                continue;
                            }
                        }

                        // diagonal down-right
                        if (i + wordLen <= gridSize && j + wordLen <= gridSize) {
                          //  boolean found = true;
                            boolean found = doDiagonalDownRight(wordLen,word,i,j);
                            if (found) {
                                results.add(word + " " + i + ":" + j + " " + (i + wordLen - 1) + ":" + (j + wordLen - 1));
                                continue;
                            }
                        }

                        // diagonal down-left
                        if (i + wordLen <= gridSize && j - wordLen >= -1) {
                            //boolean found = true;
                            boolean found = doDiagonalDownLeft(wordLen,word,i,j);
                            if (found) {
                                results.add(word + " " + i + ":" + j + " " + (i + wordLen - 1) + ":" + (j - wordLen + 1));
                                continue;
                            }
                        }

                        // diagonal up-right
                        if (i - wordLen >= -1 && j + wordLen <= gridSize) {
                            //boolean found = true;
                            boolean found = doDiagonalUpRight(wordLen,word,i,j);
                            if (found) {
                                results.add(word + " " + i + ":" + j + " " + (i - wordLen + 1) + ":" + (j + wordLen - 1));
                                continue;
                            }
                        }

                        // diagonal up-left
                        if (i - wordLen >= -1 && j - wordLen >= -1) {
                           // boolean found = true;
                            boolean found = doDiagonalUpLeft(wordLen,word,i,j);
                            if (found) {
                                results.add(word + " " + i + ":" + j + " " + (i - wordLen + 1) + ":" + (j - wordLen + 1));
                            }
                        }
                    }
                }
            }
        }

        return results;
    }



    private boolean doHorizontalRight(int wordLen, String word, int i, int j) {
        boolean flag=true;
        for (int k = 1; k < wordLen; k++) {
            if (grid[i][j + k] != word.charAt(k)) {
                flag = false;
                break;
            }
        }
        return flag;
    }

    private boolean doHorizontalLeft(int wordLen, String word, int i, int j) {
        boolean flag=true;
        for (int k = 1; k < wordLen; k++) {
            if (grid[i][j - k] != word.charAt(k)) {
                flag = false;
                break;
            }
        }
        return flag;
    }

    private boolean doVerticalDown(int wordLen, String word, int i, int j) {
        boolean flag=true;
        for (int k = 1; k < wordLen; k++) {
            if (grid[i + k][j] != word.charAt(k)) {
                flag = false;
                break;
            }
        }
        return flag;
    }

    private boolean doVerticalUp(int wordLen, String word, int i, int j) {
        boolean flag=true;
        for (int k = 1; k < wordLen; k++) {
            if (grid[i - k][j] != word.charAt(k)) {
                flag = false;
                break;
            }
        }
        return flag;
    }

    private boolean doDiagonalDownRight(int wordLen, String word, int i, int j) {
        boolean flag=true;
        for (int k = 1; k < wordLen; k++) {
            if (grid[i + k][j + k] != word.charAt(k)) {
                flag = false;
                break;
            }
        }
        return flag;
    }

    private boolean doDiagonalDownLeft(int wordLen, String word, int i, int j) {
        boolean flag=true;
        for (int k = 1; k < wordLen; k++) {
            if (grid[i + k][j - k] != word.charAt(k)) {
                flag = false;
                break;
            }
        }
        return flag;
    }

    private boolean doDiagonalUpRight(int wordLen, String word, int i, int j) {
        boolean flag=true;
        for (int k = 1; k < wordLen; k++) {
            if (grid[i - k][j + k] != word.charAt(k)) {
                flag = false;
                break;
            }
        }
        return flag;
    }

    private boolean doDiagonalUpLeft(int wordLen, String word, int i, int j) {
        boolean flag=true;
        for (int k = 1; k < wordLen; k++) {
            if (grid[i - k][j - k] != word.charAt(k)) {
                flag = false;
                break;
            }
        }
        return flag;
    }



}
