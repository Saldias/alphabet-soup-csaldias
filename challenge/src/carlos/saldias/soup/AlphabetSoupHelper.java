package carlos.saldias.soup;

import java.util.List;

public class AlphabetSoupHelper {

    public static void describeFileSource(List<String> sourceFile) {
        System.out.println(sourceFile);
    }

    public static List<String> buildWordGrid(List<String> sourceFile, int gridSize) {
        return sourceFile.subList(1, gridSize + 1);
    }

    public static List<String> wordsList(List<String> sourceFile, int gridSize) {
        return sourceFile.subList(gridSize + 1, sourceFile.size());
    }

    public static void displayFoundWordsCoordinates(List<String> results) {
        System.out.println("----- Findings report section ------");
        results.forEach(r-> System.out.println(r));
        System.out.println("-------------------------------------");
    }
}
