package carlos.saldias.soup;

import java.util.List;

public interface AlphabetSoupFactory {

    void processAlphabetSoup(List<String> sourceFile);

}
